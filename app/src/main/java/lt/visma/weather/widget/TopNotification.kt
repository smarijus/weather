package lt.visma.weather.widget

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.view_top_notification.view.*
import lt.visma.weather.R

class TopNotification @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val slideDown: Animation
    private val slideUp: Animation
    private var onCloseClick: OnClickListener? = null

    init {
        inflate(getContext(), R.layout.view_top_notification, this)
        slideDown = AnimationUtils.loadAnimation(context, R.anim.slide_down)
        slideUp = AnimationUtils.loadAnimation(context, R.anim.slide_up)
        slideUp.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                visibility = GONE
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
    }

    private fun setError(error: Int?, withAnimation: Boolean) {
        if (error == null || error == 0) {
            hide(withAnimation)
        } else {
            textView.setText(error)
            show(withAnimation)
        }
    }

    private fun setError(error: String?, withAnimation: Boolean) {
        if (error == null) {
            hide(withAnimation)
        } else {
            textView.setText(error)
            show(withAnimation)
        }
    }

    fun setError(error: String?) {
        setError(error, true)
    }

    fun setError(error: Int) {
        setError(error, true)
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        val closeButtonArea = Rect(closeButton.getLeft(), closeButton.getTop(), closeButton.getRight(), closeButton.getBottom())
        if (event.action == MotionEvent.ACTION_UP && closeButtonArea.contains(event.x.toInt(), event.y.toInt())) {
            if (onCloseClick != null) onCloseClick!!.onClick(this@TopNotification)
            setError(null as String?)
        }
        return super.dispatchTouchEvent(event)
    }

    fun setOnCloseClick(onCloseClick: OnClickListener?) {
        this.onCloseClick = onCloseClick
    }

    private fun show(withAnimation: Boolean) {
        visibility = VISIBLE
        if (withAnimation) {
            startAnimation(slideDown)
        }
    }

    private fun hide(withAnimation: Boolean) {
        if (withAnimation && visibility == VISIBLE) {
            startAnimation(slideUp)
        } else {
            visibility = GONE
        }
    }


}