package lt.visma.weather.widget

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.view_apptoolbar.view.*
import lt.visma.weather.R

class AppToolbar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    init {
        View.inflate(context, R.layout.view_apptoolbar, this)
        navigationBackButton.setOnClickListener { getActivity(context)?.onBackPressed() }
    }

    private fun getActivity(context: Context): Activity? {
        if (context is Activity) return context
        return if (context is ContextWrapper) context.baseContext as Activity else null
    }
}