package lt.visma.weather.widget

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import lt.visma.weather.R

class ProgressDialog(context: Context) : Dialog(context, R.style.ProgressDialogTheme) {
    init {
        show()
        setCancelable(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_progress)
    }

}