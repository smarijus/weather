package lt.visma.weather.helpers

import android.annotation.SuppressLint
import lt.visma.weather.model.TemperatureColor
import java.text.SimpleDateFormat
import java.util.*

object WeatherViewHelper {

    fun getTemperatureColorType(temperature: Double): TemperatureColor {
        return when {
            temperature <= TemperatureColor.COLD.tempConstMax -> TemperatureColor.COLD
            temperature <= TemperatureColor.REGULAR.tempConstMax ->  TemperatureColor.REGULAR
            else -> TemperatureColor.HOT
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun formatDate(date: Date): CharSequence {
        return SimpleDateFormat("EEE\ndd").format(date)
    }

    fun formatTemperature(temp: Double): CharSequence {
        return String.format("%.0f", temp)
    }

    fun getCloudUri(icon: String): String {
        return "https://openweathermap.org/img/wn/${icon}@2x.png"
    }
}