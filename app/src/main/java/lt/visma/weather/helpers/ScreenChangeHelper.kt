package lt.visma.weather.helpers

import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler

object ScreenChangeHelper {
    fun create(controller: Controller): RouterTransaction {
        return RouterTransaction.with(controller).apply {
            popChangeHandler(HorizontalChangeHandler())
            pushChangeHandler(HorizontalChangeHandler())
        }
    }
}