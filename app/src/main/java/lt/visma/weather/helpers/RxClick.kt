package lt.visma.weather.helpers

import android.view.View
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import timber.log.Timber
import java.util.concurrent.TimeUnit

object RxClick {

    @JvmStatic
    fun onSafeClick(view: View, onClickListener: View.OnClickListener): Disposable {
        return view.clicks()
            .throttleFirst(500, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onClickListener.onClick(view) },
                { Timber.w(it) })
    }
}