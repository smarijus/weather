package lt.visma.weather.model

import lt.visma.weather.R

enum class TemperatureColor(val cityTextColor: Int, val temperatureTextColor: Int, val dateTextColor: Int, val tempConstMax: Int) {
    COLD(R.color.weatherTextColorWarning, R.color.weatherTextColorSuccess, R.color.weatherTextColorDanger, 10),
    REGULAR(R.color.weatherTextColorDanger, R.color.weatherTextColorWarning, R.color.weatherTextColorSuccess, 19),
    HOT(R.color.weatherTextColorSuccess, R.color.weatherTextColorDanger, R.color.weatherTextColorWarning, Int.MAX_VALUE)
}

