package lt.visma.weather.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WeatherResponse(
    var weather: List<WeatherItem>?,
    var main: MainItem,
    var dt: Long,
    var name: String,
) : Parcelable

@Parcelize
data class WeatherItem(
    var description: String,
    var icon: String
) : Parcelable

@Parcelize
data class MainItem(
    var temp: Double,

    @SerializedName("temp_min")
    var tempMin: Double,

    @SerializedName("temp_max")
    var tempMax: Double
) : Parcelable