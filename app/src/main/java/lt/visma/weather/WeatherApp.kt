package lt.visma.weather

import android.app.Application
import android.content.Context
import com.facebook.drawee.backends.pipeline.Fresco
import com.swace.di.module.ApplicationModule
import lt.visma.weather.di.component.DaggerIApplicationComponent
import lt.visma.weather.di.component.IApplicationComponent
import timber.log.Timber

class WeatherApp : Application() {
    lateinit var component: IApplicationComponent

    override fun onCreate() {
        super.onCreate()
        initDager()
        Timber.plant(Timber.DebugTree())
        Fresco.initialize(this);
    }

    private fun initDager() {
        component = DaggerIApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    companion object {
        fun appComponent(context: Context): IApplicationComponent {
            return (context.applicationContext as WeatherApp).component
        }
    }
}