package lt.visma.weather.networking

import android.content.Context
import lt.visma.weather.R
import javax.inject.Inject

class ErrorHandler @Inject constructor(
    private val context: Context
) {
    fun getErrorMessage(t: Throwable): String {
        return context.getString(R.string.error_unexpected)
    }
}