package lt.visma.weather.networking

import io.reactivex.Single
import lt.visma.weather.model.WeatherResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("data/2.5/weather?units=metric")
    fun getWeatherByCity(@Query("q") cityName: String, @Query("appId") appId: String): Single<WeatherResponse>
}