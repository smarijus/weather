package lt.visma.weather.networking.repository

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import lt.ito.components.schedulers.SchedulerProvider
import lt.visma.weather.BuildConfig
import lt.visma.weather.model.WeatherResponse
import lt.visma.weather.networking.WeatherApi
import javax.inject.Inject

class WeatherRepository @Inject constructor(
    private val weatherApi: WeatherApi,
    private val schedulerProvider: SchedulerProvider
) {

    fun getWeatherByCity(cityName: String): Single<WeatherResponse> {
        return weatherApi.getWeatherByCity(cityName, BuildConfig.SERVER_APP_ID)
            .compose(schedulerProvider.applySchedulersForSingle())

    }

    fun getWeatherByCities(searches: List<String>): Single<List<WeatherResponse>> {
        val result = mutableListOf<WeatherResponse>()
        return Completable.mergeArray(
            Observable.just(searches)
                .flatMapIterable { searche -> searche }
                .flatMapCompletable {
                    getWeatherByCity(it)
                        .flatMapCompletable {
                            result.add(it);
                            Completable.complete()
                        }
                }
        ).andThen(Single.just(result))
    }
}
