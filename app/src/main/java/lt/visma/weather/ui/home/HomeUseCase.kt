package lt.visma.weather.ui.home

import lt.visma.weather.data.AppSharedPrefs
import javax.inject.Inject

class HomeUseCase @Inject constructor(
    private val appSharedPrefs: AppSharedPrefs
) {
    fun addSearch(cityName: String) {
        if(!appSharedPrefs.getLastSearches().contains(cityName)) {
            val newList = appSharedPrefs.getLastSearches().take(4).toMutableList()
            newList.add(0, cityName)
            appSharedPrefs.setLastSearches(newList)
        }
    }

    fun getSearches(): List<String> {
        return appSharedPrefs.getLastSearches();
    }
}