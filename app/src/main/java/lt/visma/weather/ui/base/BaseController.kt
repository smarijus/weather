package lt.visma.weather.ui.base

import android.os.Bundle
import com.bluelinelabs.conductor.Controller
import lt.visma.weather.helpers.KeyboardHelper
import lt.visma.weather.widget.ProgressDialog
import timber.log.Timber

abstract class BaseController(args: Bundle? = null) : Controller(args), BaseView {

    private var dialog: ProgressDialog? = null

    override fun showError(it: Throwable?, localizedMessage: String) {
        Timber.e(it, localizedMessage)
    }

    override fun hideKeyboard() {
        KeyboardHelper().hideKeyboardFrom(activity!!, view!!)
    }

    override fun getString(resId: Int): String {
        return activity?.getString(resId) ?: ""
    }

    override fun showLoadingProgress() {
        dismissDialog()
        dialog = ProgressDialog(activity!!)
    }

    override fun hideLoadingProgress() {
        dismissDialog()
    }

    private fun dismissDialog() {
        dialog?.dismiss()

    }
}