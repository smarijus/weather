package lt.visma.weather.ui.home

import lt.visma.weather.model.WeatherResponse
import lt.visma.weather.ui.base.BaseView

interface HomeView : BaseView {
    fun openWeatherList(weatherList: List<WeatherResponse>)
    fun clearCityInput()
}