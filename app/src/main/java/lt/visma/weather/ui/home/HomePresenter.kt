package lt.visma.weather.ui.home

import lt.visma.weather.R
import lt.visma.weather.base.BasePresenter
import lt.visma.weather.networking.ErrorHandler
import lt.visma.weather.networking.repository.WeatherRepository
import javax.inject.Inject

class HomePresenter @Inject constructor(
    private val errorHandler: ErrorHandler,
    private val weatherRepository: WeatherRepository,
    private val homeUseCase: HomeUseCase
) : BasePresenter<HomeView>() {

    fun onSearchClick(cityNameInput: String) {
        view?.hideKeyboard()
        if (cityNameInput.isEmpty()) {
            view?.showError(null, view?.getString(R.string.error_empty_field) ?: "")
            return
        }

        subscriptions.add(
            weatherRepository.getWeatherByCity(cityNameInput)
                .doOnSubscribe { view?.showLoadingProgress() }
                .doFinally { view?.hideLoadingProgress() }
                .subscribe(
                    {
                        homeUseCase.addSearch(it.name)
                        view?.clearCityInput()
                        view?.openWeatherList(listOf(it))
                    },
                    {
                        view?.showError(it, errorHandler.getErrorMessage(it))
                    })
        )
    }

    fun onHistoryButtonClick() {
        view?.hideKeyboard()
        subscriptions.add(
            weatherRepository.getWeatherByCities(homeUseCase.getSearches())
                .doOnSubscribe { view?.showLoadingProgress() }
                .doFinally { view?.hideLoadingProgress() }
                .subscribe(
                    {
                        view?.openWeatherList(it)
                    },
                    {
                        view?.showError(it, errorHandler.getErrorMessage(it))
                    })
        )
    }
}