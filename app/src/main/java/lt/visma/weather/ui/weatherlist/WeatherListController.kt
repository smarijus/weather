package lt.visma.weather.ui.weatherlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.controller_weather_list.view.*
import lt.visma.weather.R
import lt.visma.weather.model.WeatherResponse
import lt.visma.weather.ui.base.BaseController

class WeatherListController(
    args: Bundle
) : BaseController(args) {

    private var weatherList: List<WeatherResponse>

    constructor(weatherList: List<WeatherResponse>) : this(
        Bundle().apply { putParcelableArrayList(KEY_WEATHER_LIST, ArrayList(weatherList)) }
    )

    init {
        weatherList = args.getParcelableArrayList<WeatherResponse>(KEY_WEATHER_LIST) as List<WeatherResponse>
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return inflater.inflate(R.layout.controller_weather_list, container, false).apply {
            recyclerView.adapter = WeatherListAdapter(weatherList)
            recyclerView.layoutManager = LinearLayoutManager(context)
        }
    }

    companion object {
        const val KEY_WEATHER_LIST = "key_weather_list"
    }
}