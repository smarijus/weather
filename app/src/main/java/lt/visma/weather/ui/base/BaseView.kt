package lt.visma.weather.ui.base

interface BaseView {
    fun showError(it: Throwable?, localizedMessage: String)
    fun getString(resId: Int): String
    fun hideKeyboard()
    fun showLoadingProgress()
    fun hideLoadingProgress()
}