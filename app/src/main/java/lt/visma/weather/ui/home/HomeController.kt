package lt.visma.weather.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.controller_home.view.*
import lt.visma.weather.R
import lt.visma.weather.WeatherApp
import lt.visma.weather.helpers.RxClick
import lt.visma.weather.helpers.ScreenChangeHelper
import lt.visma.weather.model.WeatherResponse
import lt.visma.weather.ui.base.BaseController
import lt.visma.weather.ui.weatherlist.WeatherListController
import javax.inject.Inject

class HomeController : BaseController(), HomeView {

    @Inject
    lateinit var homePresenter: HomePresenter

    private val subscriptions = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        WeatherApp.appComponent(activity!!).inject(this)
        return inflater.inflate(R.layout.controller_home, container, false)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        homePresenter.view = this
        subscriptions.add(RxClick.onSafeClick(view.citySearchButton) {
            homePresenter.onSearchClick(view.cityInput.text.toString())
        })
        subscriptions.add(RxClick.onSafeClick(view.historyButton) {
            homePresenter.onHistoryButtonClick()
        })
        subscriptions.add(RxClick.onSafeClick(view.homeRoot) {
            hideKeyboard()
            view.topNotification.setError(null)
        })

    }

    override fun onDetach(view: View) {
        super.onDetach(view)
        homePresenter.view = null
        subscriptions.clear()
        view.citySearchButton.setOnClickListener(null)
        view.historyButton.setOnClickListener(null)
    }

    override fun openWeatherList(weatherList: List<WeatherResponse>) {
        router.pushController(ScreenChangeHelper.create(WeatherListController(weatherList)))
    }

    override fun showError(it: Throwable?, localizedMessage: String) {
        view?.topNotification?.setError(localizedMessage)
    }

    override fun clearCityInput() {
        view?.cityInput?.text = null
    }

}