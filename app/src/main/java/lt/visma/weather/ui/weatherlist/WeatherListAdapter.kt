package lt.visma.weather.ui.weatherlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_weather_card.view.*
import lt.visma.weather.R
import lt.visma.weather.helpers.WeatherViewHelper
import lt.visma.weather.model.WeatherResponse
import java.util.*

class WeatherListAdapter constructor(
    private val weatherList: List<WeatherResponse>
) : RecyclerView.Adapter<WeatherListAdapter.ViewHolder>() {

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_weather_card, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = weatherList.get(position)
        val weatherItem = item.weather?.first()
        val context = holder.itemView.context

        holder.itemView.temperatureView.text = WeatherViewHelper.formatTemperature(item.main.temp)
        holder.itemView.cityNameView.text = item.name
        holder.itemView.dayTimeView.text = WeatherViewHelper.formatDate(Date(item.dt * 1000))

        val temperatureColor = WeatherViewHelper.getTemperatureColorType(item.main.temp)
        holder.itemView.temperatureView.setTextColor( ContextCompat.getColor(context, temperatureColor.temperatureTextColor))
        holder.itemView.temperatureUnitView.setTextColor( ContextCompat.getColor(context, temperatureColor.temperatureTextColor))
        holder.itemView.cityNameView.setTextColor( ContextCompat.getColor(context, temperatureColor.cityTextColor))
        holder.itemView.dayTimeView.setTextColor( ContextCompat.getColor(context, temperatureColor.dateTextColor))

        if (weatherItem != null) {
            holder.itemView.weatherDescriptionView.text = weatherItem.description.capitalize(Locale.getDefault())
            holder.itemView.cloudImage.setImageURI(WeatherViewHelper.getCloudUri(weatherItem.icon))
            holder.itemView.weatherDescriptionView.visibility = View.VISIBLE
            holder.itemView.cloudImage.visibility = View.VISIBLE
        } else {
            holder.itemView.weatherDescriptionView.visibility = View.GONE
            holder.itemView.cloudImage.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int = weatherList.size
}

