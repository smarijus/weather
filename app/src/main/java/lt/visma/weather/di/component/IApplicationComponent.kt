package lt.visma.weather.di.component

import com.swace.di.module.ApplicationModule
import dagger.Component
import lt.visma.weather.WeatherApp
import lt.visma.weather.ui.MainActivity
import lt.visma.weather.ui.home.HomeController
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface IApplicationComponent {
    fun getRetrofit(): Retrofit

    fun inject(app: WeatherApp)
    fun inject(app: MainActivity)
    fun inject(homeController: HomeController)
}