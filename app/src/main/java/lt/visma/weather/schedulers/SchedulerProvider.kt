package lt.ito.components.schedulers

import io.reactivex.CompletableTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import io.reactivex.SingleTransformer

interface SchedulerProvider {
    fun ui(): Scheduler

    fun computation(): Scheduler

    fun io(): Scheduler

    fun <T> applySchedulers(): ObservableTransformer<T, T>

    fun applySchedulersForCompletable(): CompletableTransformer
    fun <T> applySchedulersForSingle(): SingleTransformer<T, T>
}