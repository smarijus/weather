package com.swace.schedulers

import io.reactivex.CompletableTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import lt.ito.components.schedulers.SchedulerProvider

class AppSchedulerProvider : SchedulerProvider {

    override fun ui(): Scheduler = AndroidSchedulers.mainThread()

    override fun computation(): Scheduler = Schedulers.computation()

    override fun io(): Scheduler = io

    override fun <T> applySchedulers(): ObservableTransformer<T, T> {
        return ObservableTransformer {
            it.subscribeOn(io).observeOn(ui())
        }
    }

    override fun applySchedulersForCompletable(): CompletableTransformer {
        return CompletableTransformer {
            it.subscribeOn(io).observeOn(ui())
        }
    }

    override fun <T> applySchedulersForSingle(): SingleTransformer<T, T> {
        return SingleTransformer {
            it.subscribeOn(io).observeOn(ui())
        }
    }

    companion object {
        var io: Scheduler = Schedulers.io()
    }
}