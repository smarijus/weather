package lt.visma.weather.data

import android.content.SharedPreferences
import javax.inject.Inject

class AppSharedPrefs @Inject constructor(
    private val pref: SharedPreferences
) {
    private var lastSearches: List<String>

    init {
         lastSearches = pref.getStringSet(KEY_LAST_SEARCHES, null)?.toList() ?: emptyList()
    }

    fun getLastSearches(): List<String> {
        return lastSearches
    }

    fun setLastSearches(lastSearches: List<String>) {
        this.lastSearches = lastSearches
        pref.edit().putStringSet(KEY_LAST_SEARCHES, lastSearches.toSet()).apply()
    }

    companion object {
        const val PREFS_NAME = "app_prefs"
        const val KEY_LAST_SEARCHES = "key_last_searches"
    }
}