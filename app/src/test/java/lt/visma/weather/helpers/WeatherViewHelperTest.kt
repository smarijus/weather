package lt.visma.weather.helpers

import lt.visma.weather.model.TemperatureColor
import org.junit.Assert.assertEquals
import org.junit.Test
import java.text.SimpleDateFormat

class WeatherViewHelperTest {

    @Test
    fun getTemperatureColorType_whenCold() {
        assertEquals(TemperatureColor.COLD, WeatherViewHelper.getTemperatureColorType(Double.MIN_VALUE))
        assertEquals(TemperatureColor.COLD, WeatherViewHelper.getTemperatureColorType(0.000))
        assertEquals(TemperatureColor.COLD, WeatherViewHelper.getTemperatureColorType(10.000))
        assertEquals(TemperatureColor.COLD, WeatherViewHelper.getTemperatureColorType(TemperatureColor.COLD.tempConstMax.toDouble()))
    }

    @Test
    fun getTemperatureColorType_whenRegular() {
        assertEquals(TemperatureColor.REGULAR, WeatherViewHelper.getTemperatureColorType(10.01))
        assertEquals(TemperatureColor.REGULAR, WeatherViewHelper.getTemperatureColorType(19.00))
        assertEquals(TemperatureColor.REGULAR, WeatherViewHelper.getTemperatureColorType(TemperatureColor.REGULAR.tempConstMax.toDouble()))
    }

    @Test
    fun getTemperatureColorType_whenHot() {
        assertEquals(TemperatureColor.HOT, WeatherViewHelper.getTemperatureColorType(19.01))
        assertEquals(TemperatureColor.HOT, WeatherViewHelper.getTemperatureColorType(Double.MAX_VALUE))
        assertEquals(TemperatureColor.HOT, WeatherViewHelper.getTemperatureColorType(TemperatureColor.HOT.tempConstMax.toDouble()))
    }

    @Test
    fun formatDate() {
        val currentDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse("2019-01-01T00:00:00+0200")
        assertEquals("Tue\n01", WeatherViewHelper.formatDate(currentDate))
    }

    @Test
    fun formatTemperature() {
        assertEquals("12", WeatherViewHelper.formatTemperature(12.345))
        assertEquals("-1", WeatherViewHelper.formatTemperature(-1.345))
    }

    @Test
    fun getCloudUri() {
        assertEquals("https://openweathermap.org/img/wn/customIcon@2x.png", WeatherViewHelper.getCloudUri("customIcon"))
    }
}