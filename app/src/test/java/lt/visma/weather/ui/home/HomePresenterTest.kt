package lt.visma.weather.ui.home

import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import lt.visma.weather.R
import lt.visma.weather.model.WeatherResponse
import lt.visma.weather.networking.ErrorHandler
import lt.visma.weather.networking.repository.WeatherRepository
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.HttpException

class HomePresenterTest {

    @Mock
    lateinit var homeView: HomeView

    @Mock
    lateinit var errorHandler: ErrorHandler

    @Mock
    lateinit var weatherRepository: WeatherRepository

    @Mock
    lateinit var homeUseCase: HomeUseCase

    private lateinit var homePresenter: HomePresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        homePresenter = HomePresenter(errorHandler, weatherRepository, homeUseCase)
        homePresenter.view = homeView
        whenever(errorHandler.getErrorMessage(any())).thenReturn("errorMessage")
        whenever(homeView.getString(R.string.error_empty_field)).thenReturn("errorEmptyField")
    }

    @Test
    fun onSearchClick_whenInputEmpty() {
        homePresenter.onSearchClick("")

        verify(homeView, never()).showLoadingProgress()
        verify(homeView, never()).hideLoadingProgress()
        verify(homeView).showError(anyOrNull(), eq("errorEmptyField"))
        verify(homeView).hideKeyboard()
    }

    @Test
    fun onSearchClick_whenHttpError() {
        val apiError = Mockito.mock(HttpException::class.java)
        whenever(weatherRepository.getWeatherByCity(any())).thenReturn(Single.error(apiError))
        homePresenter.onSearchClick("kaunas")

        verify(homeView).showLoadingProgress()
        verify(homeView).hideLoadingProgress()
        verify(homeView).hideKeyboard()
        verify(homeView).showError(anyOrNull(), eq("errorMessage"))
    }

    @Test
    fun onSearchClick_whenSuccess() {
        val apiResponse = Mockito.mock(WeatherResponse::class.java)
        whenever(weatherRepository.getWeatherByCity(any())).thenReturn(Single.just(apiResponse))
        homePresenter.onSearchClick("kaunas")

        verify(homeView).showLoadingProgress()
        verify(homeView).hideLoadingProgress()
        verify(homeView).hideKeyboard()
        verify(homeView, never()).showError(anyOrNull(), eq("errorMessage"))
        verify(homeView).openWeatherList(any())
        verify(homeView).clearCityInput()
        verify(homeUseCase).addSearch(anyOrNull())
    }

    @Test
    fun onHistoryButtonClick_whenEmpty() {
        whenever(homeUseCase.getSearches()).thenReturn(emptyList())
        whenever(weatherRepository.getWeatherByCities(emptyList())).thenReturn(Single.just(emptyList()))
        homePresenter.onHistoryButtonClick()

        verify(homeView).showLoadingProgress()
        verify(homeView).hideLoadingProgress()
        verify(homeView).hideKeyboard()
        verify(homeView, never()).showError(anyOrNull(), eq("errorMessage"))
        verify(homeView).openWeatherList(emptyList())
    }

    @Test
    fun onHistoryButtonClick_whenHaveSearches() {
        val searches = listOf("Kaunas", "Vilnius")
        val response = listOf(Mockito.mock(WeatherResponse::class.java))
        whenever(homeUseCase.getSearches()).thenReturn(searches)
        whenever(weatherRepository.getWeatherByCities(searches)).thenReturn(Single.just(response))
        homePresenter.onHistoryButtonClick()

        verify(homeView).showLoadingProgress()
        verify(homeView).hideLoadingProgress()
        verify(homeView).hideKeyboard()
        verify(homeView, never()).showError(anyOrNull(), eq("errorMessage"))
        verify(homeView).openWeatherList(response)
    }

    @Test
    fun onHistoryButtonClick_whenApiError() {
        val searches = listOf("Kaunas", "Vilnius")
        val apiError = Mockito.mock(HttpException::class.java)
        whenever(homeUseCase.getSearches()).thenReturn(searches)
        whenever(weatherRepository.getWeatherByCities(searches)).thenReturn(Single.error(apiError))
        homePresenter.onHistoryButtonClick()

        verify(homeView).showLoadingProgress()
        verify(homeView).hideLoadingProgress()
        verify(homeView).hideKeyboard()
        verify(homeView).showError(anyOrNull(), eq("errorMessage"))
        verify(homeView, never()).openWeatherList(anyOrNull())
    }
}